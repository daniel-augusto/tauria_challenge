FROM python:3.8-alpine

RUN apk add --update postgresql-dev libffi-dev openssl-dev musl-dev make g++ gcc

WORKDIR /app

COPY ./requirements.txt /tmp/requirements.txt
RUN cd /tmp/ && pip install --no-cache-dir --disable-pip-version-check --require-hashes -r requirements.txt

COPY . /app

CMD ["gunicorn", "tauria_challenge.wsgi"]
