from rest_framework import routers

from core.views import ReadOnlyUserViewSet, RoomViewSet

router = routers.SimpleRouter()

router.register('users', ReadOnlyUserViewSet, basename='user')
router.register('rooms', RoomViewSet, basename='room')

urlpatterns = router.urls
