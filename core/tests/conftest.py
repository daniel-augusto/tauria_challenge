import pytest

from core.models import User, Room


@pytest.fixture(autouse=True)
def enable_db_access(db):
    pass


@pytest.fixture
def host():
    return User.objects.create_user(username='test_user')


@pytest.fixture
def user():
    return User.objects.create_user(username='new.user')


@pytest.fixture
def room(host):
    room = Room.objects.create(
        name='Test Chamber',
        host=host,
        capacity=5,
    )
    room.participants.add(host)
    return room
