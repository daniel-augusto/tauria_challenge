import pytest
from django.core.exceptions import ValidationError

from core.models import Room


class TestRoom:
    def test_add_user_to_a_full_room(self, room, user):
        room.capacity = 1
        room.save()

        with pytest.raises(ValidationError):
            room.add_user(user)

        assert room.occupation == 1

    def test_add_user_succesfully(self, room, host, user):
        room.add_user(user)

        participants = room.participants.all()
        assert host in participants
        assert user in participants
        assert room.occupation == 2

    def test_remove_user_succesfully(self, room, user):
        room.participants.add(user)
        occupation = room.occupation

        room.remove_user(user)

        assert room.occupation == occupation - 1
        assert user not in room.participants.all()

    def test_remove_user_that_is_not_on_the_room(self, room, user):
        occupation = room.occupation

        room.remove_user(user)

        assert room.occupation == occupation
        assert user not in room.participants.all()

    def test_create_room(self, host):
        room = Room.create_room(name='test', host=host)

        assert room.name == 'test'
        assert room.capacity == 5
        assert room.host == host
        assert room.participants.get() == host
