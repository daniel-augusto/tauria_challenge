import json

import pytest
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.reverse import reverse

from core.models import User


class TestUserViewSet:
    @pytest.fixture(autouse=True)
    def users(self):
        return [
            User.objects.create_user(
                username=f"user{index}",
                mobile_token=f"{index}",
                first_name=f'Jane {index}',
                last_name='Doe',
            ) for index in range(5)
        ]

    def test_user_list(self, client):
        response = client.get(reverse("user-list"))

        assert len(response.json()) == 5

    def test_user_detail(self, client):
        response = client.get(reverse("user-detail", kwargs={'username': 'user1'}))
        data = response.json()

        assert data["username"] == "user1"
        assert len(data) == 1

    def test_update_user_not_logged_in(self, client):
        response = client.patch(
            reverse("user-detail", kwargs={'username': 'user1'}),
            data={'password': 'new_password'}
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_update_user_successfully(self, client, users):
        user = users[0]
        old_password = user.password
        client.force_login(user)

        response = client.patch(
            reverse("user-detail", kwargs={'username': 'user0'}),
            data=json.dumps({'mobile_token': '1234', 'password': '5678'}),
            content_type="application/json",
        )
        user.refresh_from_db()

        assert response.status_code == status.HTTP_200_OK
        assert user.mobile_token == '1234'
        assert old_password != user.password
        # To make sure that it's not saving the plan password
        assert user.password != '5678'

    def test_try_to_update_another_user(self, client, users):
        user = users[0]
        client.force_login(user)

        response = client.patch(
            reverse("user-detail", kwargs={'username': 'user1'}),
            data=json.dumps({}), content_type="application/json"
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_try_to_delete_another_user(self, client, users):
        user = users[0]
        client.force_login(user)

        response = client.delete(
            reverse("user-detail", kwargs={'username': 'user1'})
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_delete_user_successfully(self, client, users):
        user = users[0]
        client.force_login(user)

        response = client.delete(
            reverse("user-detail", kwargs={'username': 'user0'})
        )

        with pytest.raises(ObjectDoesNotExist):
            user.refresh_from_db()

        assert not User.objects.filter(username='user0').exists()

        assert response.status_code == status.HTTP_204_NO_CONTENT
