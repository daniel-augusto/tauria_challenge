from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse


User = get_user_model()


class TestRegistration:
    def test_registration(self, client, mocker):
        data = {
            'password1': 'testing1239',
            'password2': 'testing1239',
            'username': 'jane_doe',
            'mobile_token': '1234',
        }

        response = client.post(reverse('rest_register'), data=data)

        assert response.json() == {'key': mocker.ANY}

        user = User.objects.get(username='jane_doe')
        assert user.mobile_token == '1234'

    def test_registration_without_mobile_token(self, client, mocker):
        data = {
            'password1': 'testing1239',
            'password2': 'testing1239',
            'username': 'jane_doe',
        }

        response = client.post(reverse('rest_register'), data=data)

        assert response.json() == {'key': mocker.ANY}

        user = User.objects.get(username='jane_doe')
        assert user.mobile_token == ''
