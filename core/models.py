import uuid

from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.db import models


class User(AbstractUser):
    """Swap user model to add department field"""
    mobile_token = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.username


class Room(models.Model):
    name = models.CharField(max_length=100)
    guid = models.UUIDField(
        unique=True,
        db_index=True,
        default=uuid.uuid4,
        editable=False,
    )
    host = models.ForeignKey(User, related_name='hosting_rooms', on_delete=models.PROTECT)
    participants = models.ManyToManyField(User)
    capacity = models.IntegerField(default=5)

    @classmethod
    def create_room(cls, name, host, capacity=5):
        room = cls.objects.create(
            name=name,
            host=host,
            capacity=capacity or 5,
        )
        room.add_user(host)
        return room

    @property
    def occupation(self):
        return self.participants.count()

    def add_user(self, user):
        if self.occupation >= self.capacity:
            raise ValidationError('The room is already full.')

        self.participants.add(user)

    def remove_user(self, user):
        self.participants.remove(user)
